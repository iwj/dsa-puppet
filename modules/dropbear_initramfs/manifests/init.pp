# this configures the initramfs on a system to be remotely accessible
#
# The $content and $source parameters refer to the authorized_keys
# deployed in the initramfs.
class dropbear_initramfs(
  Enum['present', 'absent'] $ensure = 'present',
  Optional[String] $content         = undef,
  Optional[String] $source          = undef,
) {
  package {
    'dropbear-initramfs':
      ensure => $ensure;
  }

  file {
    '/etc/dropbear-initramfs/authorized_keys':
      ensure  => $ensure,
      content => $content,
      source  => $source,
      mode    => '0444',
      require => Package['dropbear-initramfs'],
      notify  => Exec['update-initramfs -u'];
  }
}
# vim:set et:
# vim:set sts=2 ts=2:
# vim:set shiftwidth=2:
