# debtags.debian.org role
#
# @param db_address     hostname of the postgres server for this service
# @param db_port        port of the postgres server for this service
class roles::debtags (
  String  $db_address,
  Integer $db_port,
) {
  include apache2
  include apache2::ssl
  include roles::sso_rp

  package { 'libapache2-mod-wsgi-py3': ensure => installed, }
  apache2::module { 'wsgi': require => Package['libapache2-mod-wsgi-py3'] }

  ssl::service { 'debtags.debian.org':
    notify => Exec['service apache2 reload'],
    key    => true,
  }

  apache2::site { '010-debtags.debian.org':
    site   => 'debtags.debian.org',
    source => 'puppet:///modules/roles/debtags/debtags.debian.org',
  }

  @@postgres::cluster::hba_entry { "debtags-${::fqdn}":
    tag      => "postgres::cluster::${db_port}::hba::${db_address}",
    pg_port  => $db_port,
    database => 'debtags',
    user     => 'debtags',
    address  => $base::public_addresses,
  }
}
