class roles::salsa {
  include salsa

  # tell the mail-relays to forward this domain to us
  exim::manualroute{ 'salsa.debian.org': }
}
